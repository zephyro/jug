// Nav

(function() {

    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav_open');
    });

}());

(function() {

    $('.resort_mobile__nav').on('click', function(e){
        e.preventDefault();
        var check = $(this).closest('li').hasClass('active');

        if (check) {
            $(this).closest('.resort_mobile').find('>li').removeClass('active');
            $(this).closest('li').find('.resort_mobile__content').slideUp('fast');
        }
        else {
            $(this).closest('.resort_mobile').find('>li').removeClass('active');
            $(this).closest('li').addClass('active');
            $(this).closest('.resort_mobile').find('.resort_mobile__content').slideUp('fast');
            $(this).closest('li').find('.resort_mobile__content').slideDown('fast');
        }

    });

}());

(function() {

    $('.resort_desktop__nav a').on('click', function(e){
        e.preventDefault();
        var box = $('.resort_desktop');
        var tab = $($(this).attr("data-target"));
        $(this).closest('.resort_desktop__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.resort_desktop__tiles').removeClass('invert');
        box.find('.resort_desktop__item').removeClass('active');
        box.find(tab).addClass('active');
    });

    $('.resort_cat').on('click', function(e){
        e.preventDefault();
        $(this).closest('.resort_desktop__tiles').addClass('invert');
    });

}());



var navbar = new Swiper('.nav__fluid_slider', {
    loop: true,
    slidesPerView: 'auto',
    spaceBetween: 20,
    navigation: {
        nextEl: '.nav_next',
        prevEl: '.nav_prev'
    }
});


var object = new Swiper('.objects__slider', {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    navigation: {
        nextEl: '.objects__arrow_next',
        prevEl: '.objects__arrow_prev',
    },
    breakpoints: {
        768: {
            slidesPerView: 2
        },
        1024: {
            slidesPerView: 3
        },
        1230: {
            slidesPerView: 4
        }
    }
});

var comments = new Swiper('.comments__slider', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: '.comments__arrow_next',
        prevEl: '.comments__arrow_prev'
    },
    breakpoints: {
        768: {
            slidesPerView: 2
        },
        1024: {
            slidesPerView: 3
        },
        1230: {
            slidesPerView: 4
        }
    }
});


var promo = new Swiper('.promo__slider', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.promo__arrow_next',
        prevEl: '.promo__arrow_prev',
    },
});


// Hotel Gallery

var hotelThumbs = new Swiper('.hotel_main__thumbs_slider', {
    spaceBetween: 6,
    slidesPerView: 6,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
        768: {
            slidesPerView: 6,
            spaceBetween: 6,
        },
        1230: {
            slidesPerView: 9,
            spaceBetween: 22,
        },
    }
});
var hotelGallery = new Swiper('.hotel_main__gallery_slider', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.slide_nav_next',
        prevEl: '.slide_nav_prev',
    },
    thumbs: {
        swiper: hotelThumbs
    }
});

$('.thumbs_nav_next').on('click', function(e){
    e.preventDefault();
    hotelGallery.slideNext(600);
});


// Room Gallery

var roomThumbs = new Swiper('.hotel_room__thumbs_slider', {
    spaceBetween: 8,
    slidesPerView: 'auto',
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
        768: {
            slidesPerView: 6,
            spaceBetween: 8,
        }
    }
});
var roomGallery = new Swiper('.hotel_room__gallery_slider', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: roomThumbs
    }
});




// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

(function() {

    $('.side_contact__close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.side_contact').hide();
    });

}());




